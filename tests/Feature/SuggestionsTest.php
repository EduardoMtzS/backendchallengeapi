<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class SuggestionsTest extends TestCase
{

    use RefreshDatabase;

    /** @test */
    public function testInvalidParam()
    {
        $this->json('GET', '/api?param=test')
            ->assertStatus(400)
            ->assertJson([
                ["message" => "Invalid key: param"]
            ]);
    }

    /** @test */
    public function testInvalidCity()
    {
        $this->json('GET', '/api?city=zap0pan')
            ->assertStatus(400)
            ->assertJson([
                [
                    "city" => [
                        "The city format is invalid."
                    ]
                ]
            ]);
    }

    /** @test */
    public function testInvalidLatLong()
    {
        $this->json('GET', '/api?latlong=0,0')
            ->assertStatus(400)
            ->assertJson([
                "message" => "Nothing to geocode"
            ]);

        $this->json('GET', '/api?latlong=a,a')
            ->assertStatus(400)
            ->assertJson([
                [
                    "latlong" => [
                        "The latlong format is invalid."
                    ]
                ]
            ]);
    }

    /** @test */
    public function testValidRequest()
    {
        $this->json('GET', '/api?city=zapopan')
            ->assertStatus(200)
            ->assertJsonFragment([
                "limit" => 40
            ]);
    }
}
