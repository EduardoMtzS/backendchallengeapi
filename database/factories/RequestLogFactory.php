<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\RequestLog;
use Faker\Generator as Faker;

$factory->define(RequestLog::class, function (Faker $faker) {
    return [
        'cities_id' => factory(App\City::class),
        'temperature' => $faker->randomDigit,
        'tracks' => $faker->text
    ];
});
