<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\City;
use Faker\Generator as Faker;
use phpDocumentor\Reflection\Types\Null_;

$factory->define(City::class, function (Faker $faker) {
    return [
        'city' => $faker->state,
        'lat' => $faker->randomFloat(null, 0, 1000),
        'long' => $faker->randomFloat(null, 0, 1000)
    ];
});
