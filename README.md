## Back End Challenge 
Este challenge fue solucionado mediante Laravel, utilizando los patrones que vienen por defecto en el framework.

Para hacer el consumo de las apis se hizo uso de la librería guzzle que en la versión 7 de laravel viene integrado
en cierta manera.

La api se ejecuta dentro de un contenedor de Docker.

La solución fue lo más sencilla posible, al recibir los query strings son procesados para poder obtener la temperatura,
ya con la temperatura se establece el genero musical que será utilizado para obtener las canciones. Se consume la api 
de Spotify para obtener las canciones, ya con en ellas se regresa en formato JSON al cliente.

## Pre-requisitos

1. Tener instalado docker y docker-compose.

## Instrucciones de instalación

1. Después de haber clonado el repositorio, ingresar a la carpeta "BackEndChallengeApi". ``` cd BackEndChallengeApi ```.

2. Configurar archivo .env: 
``` 
...
APP_DEBUG=true
APP_URL=http://localhost:3000

...
DB_CONNECTION=mysql
DB_HOST=mysql-db
DB_PORT=3306
DB_DATABASE=backendchallengeapi
DB_USERNAME=eduardo
DB_PASSWORD=backendchallenge

...
MIX_PUSHER_APP_CLUSTER="${PUSHER_APP_CLUSTER}"

UID=1000

OPENWEATHER_CLIENT=7da5b758b2f9176fa6aedf47f21b1a03
OPENWEATHER_BASEURI=http://api.openweathermap.org/data/2.5/weather
SPOTIFY_CLIENT=MDFlYWM0YTUyNTdjNDA5NDlkODY2OGYyODRhZjEzNjY6YTc2Yjg0NTI3OTNlNDNkZmEyOGRiZTNiYjI4YThkMjQ=
SPOTIFYTOKEN_URI=https://accounts.spotify.com/api/token
SPOTIFY_BASEURI=https://api.spotify.com/v1
```

3. Instalar composer: ``` composer install ```

4. Construir servicios: ``` sudo docker-compose build ```

5. Crear e iniciar contenedores: ``` sudo docker-compose up -d ```

6. Después de que los contenedores han sido iniciados se debe correr los siguientes comandos para la correcta ejecución de la aplicación:
``` 
sudo docker-compose exec laravel-app php artisan key:generate
sudo docker-compose exec laravel-app php artisan migrate 
```

7. Para ejecutar las pruebas se utiliza el siguiente comando: 
``` 
sudo docker-compose exec laravel-app vendor/bin/phpunit 
```

## Instrucciones de uso

1. En la api se pueden recibir los parametros: 
    - city: El nombre de la ciudad donde se quieren obtener las canciones. Ej. /api?city=zapopan
    - latlong: La latitud y longitud de la ciudad donde se quieren obtener las canciones. Ej. /api?latlong=20.72,-103.4
    - limit: El limite de canciones que se quieren obtener, como minimo 1. Ej. /api?limit=5
2. Para el consumo de la api se debe usar la siguiente url:
    ``` http://localhost:3000/api?city=zapopan ```

## Formato de respuesta 

Al ser consumida de manera correcta la API regresa una respuesta JSON con la siguiente estructura:
```
{
    "limit": "5",
    "genre": "party",
    "temperature": 30.52,
    "tracks": [
        "Loyal",
        "Wasted",
        "Stay High - Habits Remix",
        "We Can't Stop",
        "Stay The Night - featuring Hayley Williams of Paramore"
    ]
}
```

## Respuesta de preguntas

1. Threading: Cómo maximizar el uso de los recursos? 
    En esta parte no tengo experiencia maximizando los recursos de la aplicación, es algo que tengo que aprender.

2. Concurrencia: Como los request concurrentes son administrados para maximizar el desempeño?
    Se esta administrando la concurrencia desde el ruteo, se utiliza un middleware incorporado por laravel que solo da 
    acceso a 60 peticiones por minuto a cada usuario. Este es el código que se utilizó:
    ```
    Route::middleware('throttle:60,1')->get('/', 'Api\SuggestionsController@show');
    ```
4. Testing: Como diseñas las pruebas para incrementar la cobertura. Agregar las pruebas unitarias
    Al tratarse de una api me enfoque más en las pruebas de "feature" en donde se prueban los endpoints para saber que se 
    están utilizando de manera correcta y a su vez están dando las respuestas esperadas.

5. Tolerancia a fallos: Como el servicio administra, aísla y reacciona a fallos?
    La tolerancia a fallos la estoy utilizando mediante un try catch en el controller principal para poder guardar
    los logs de que fue lo que sucedio. Realmente no tengo mucha experiencia sobre esto pero es algo que he empezado a aprender.

8. Usar: Coding: Conforme al estándar PSR-2 y pasar phpstand en modo estricto