<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RequestLog extends Model
{
    protected $guarded = [];

    public function city()
    {
        return $this->belongsTo('App\City', 'cities_id');
    }
}
