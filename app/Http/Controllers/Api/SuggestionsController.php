<?php

namespace App\Http\Controllers\Api;

use App\City;
use App\Http\Controllers\Controller;
use App\RequestLog;
use Exception;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class SuggestionsController extends Controller
{
    use Requests;

    protected $message = '';
    protected $genre = '';
    protected $queryParams = ['city', 'latlong', 'limit'];
    protected $limit = 40;
    protected $degrees = 0;

    public function show(Request $request)
    {
        $request = $request->all();
        if (!$this->isValidTheRequest($request, $this->queryParams, $this->message)) {
            return response()->json([
                $this->message
            ], 400);
        }

        $temperature = $this->getTemperature($request);

        if (key_exists('limit', $request)) {
            $this->limit = $request['limit'];
        }

        if ($temperature['cod'] != 200) {
            return response()->json([
                'message' => $temperature['message']
            ], $temperature['cod']);
        }

        $this->degrees = $temperature['main']['temp'];

        $playlist = $this->getPlaylist($this->degrees, $this->limit, $this->genre);

        $this->createLog($temperature, $playlist);

        return response()->json([
            'limit' => $this->limit,
            'genre' => $this->genre,
            'temperature' => $this->degrees,
            'tracks' => $playlist
        ], 200);
    }

    protected function isValidTheRequest($request, $keys, &$message)
    {
        foreach ($request as $key => $value) {
            if (!in_array($key, $keys)) {
                $message = ['message' => "Invalid key: $key"];
                return false;
            }
        }

        $validator = Validator::make($request, [
            'city' => 'regex:/^[a-zA-Z]+(?:[\s-][a-zA-Z]+)*$/',
            'latlong' => 'regex:/^(-?\d+(\.\d+)?),\s*(-?\d+(\.\d+)?)$/',
            'limit' => 'integer|min:1'
        ]);

        if ($validator->fails()) {
            $message = $validator->errors();
            return false;
        }

        return true;
    }

    protected function createLog($temperature, $tracks): void
    {
        try {
            $city = City::firstOrCreate(
                ['city' => $temperature['name']],
                [
                    'lat' => $temperature['coord']['lat'],
                    'long' => $temperature['coord']['lon']
                ]
            );

            RequestLog::create([
                'cities_id' => $city->id,
                'temperature' => $temperature['main']['temp'],
                'tracks' => json_encode($tracks)
            ]);
        } catch (Exception $e) {
            Log::info('Error creating city and requestLog' . $e);
        }
    }
}
