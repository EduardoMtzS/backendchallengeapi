<?php

namespace App\Http\Controllers\Api;

use Illuminate\Support\Facades\Http;

trait Requests
{

    public function getTemperature($queryString)
    {
        $params = ['units' => 'metric', 'appid' => env('OPENWEATHER_CLIENT')];

        if (array_key_exists('city', $queryString)) {
            $params['q'] = $queryString['city'];
        }

        if (array_key_exists('latlong', $queryString)) {
            $latlang = explode(',', $queryString['latlong']);
            $params['lat'] = trim($latlang[0]);
            $params['long'] = trim($latlang[1]);
        }

        $response = Http::get(env('OPENWEATHER_BASEURI'), $params);

        return $response->json();
    }

    public function getPlaylist($temperature, $limit, &$genre)
    {
        $params = ['limit' => $limit];
        $token = $this->getSpotifyToken();

        if ($temperature > 30) {
            $params['seed_genres'] = 'party';
        }
        if ($temperature >= 15 && $temperature <= 30) {
            $params['seed_genres'] = 'pop';
        }
        if ($temperature >= 10 && $temperature < 15) {
            $params['seed_genres'] = 'rock';
        }
        if ($temperature < 10) {
            $params['seed_genres'] = 'classical';
        }

        $genre = $params['seed_genres'];

        $playlist = $this->getTracks($token, $params);

        return $playlist;
    }

    public function getSpotifyToken()
    {
        $headers = [
            'Authorization' => 'Basic ' . env('SPOTIFY_CLIENT')
        ];
        $body = ['grant_type' => 'client_credentials'];

        $response = Http::asForm()->withHeaders($headers)->post(env('SPOTIFYTOKEN_URI'), $body);

        return $response->json()['access_token'];
    }

    public function getTracks($token, $params)
    {
        $tracks = [];
        $response = Http::withToken($token)->get(env('SPOTIFY_BASEURI') . '/recommendations', $params);

        foreach ($response->json()['tracks'] as $track) {
            $tracks[] = $track['name'];
        }

        return $tracks;
    }
}
